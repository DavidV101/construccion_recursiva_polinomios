# construccion_recursiva_polinomios
Sea 
p_1(x) = x-2
p_2(x) = x**2+x
p_n(x) = p_{n-1}(x/2) - x*p_{n-2}(x/4)

Generar los primeros 50 polinomios